﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Animator ch_animator;
    public static int poins;
    public int coins;
    public Text distance,finalScore, txtCoins;
    public GameObject pnlTouthController;
    public Canvas canvasEndGame;
    public float maxSpeed = 10f;
    public static float speed = 0;
    public Camera cam;
    public AudioSource soundCoin, soundDead;

    // Start is called before the first frame update
    void Start()
    {
        canvasEndGame.gameObject.SetActive(false);
        speed = maxSpeed;
        ch_animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (poins < 7)
        {
            speed = 5;
        }
        else if (CarController.speed != 0)
        {
            speed = 13;
        }
        gameObject.transform.position += new Vector3(0f, 0f, speed * Time.deltaTime);
        cam.transform.position += new Vector3(0f, 0f, speed * Time.deltaTime);

    }


    private void FixedUpdate()
    {
        poins = Convert.ToInt32(gameObject.transform.position.z + 265);
        distance.text = (poins).ToString() + "М";
        txtCoins.text = coins.ToString();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("DestroyRoad"))
        {
            RoadGenerator.onTrigger = true;
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            ch_animator.SetBool("Die", true);
            soundDead.GetComponent<AudioSource>().Play();
            CarController.speed = 0;
            speed = 0;
            finalScore.text = distance.text;
            pnlTouthController.gameObject.SetActive(false);
            canvasEndGame.gameObject.SetActive(true);
        }
        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
            coins++;
            soundCoin.GetComponent<AudioSource>().Play();
        }

    }
}
