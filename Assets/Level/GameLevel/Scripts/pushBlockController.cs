﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pushBlockController : MonoBehaviour
{
    public int hp;
    public static Vector3 playerScale;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (hp == 0)
        {
            gameObject.GetComponent<BoxCollider>().isTrigger = false;
            CancelInvoke();
        }

      
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            InvokeRepeating("ChangeHp", 0, 1);
            playerScale = other.gameObject.GetComponent<Transform>().localScale;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CancelInvoke();
        }
    }

    private void ChangeHp()
    {
        if (hp > 0)
        {
            hp--;
            PlayerController.poins--;
            playerScale -= new Vector3(0.1f, 0.1f, 0.1f);
        }
       
    }
}
