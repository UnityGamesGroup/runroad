﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : MonoBehaviour
{
    public GameObject RoadPrefab, carPrefab;
    private List<GameObject> roads = new List<GameObject>();
    private List<GameObject> cars = new List<GameObject>();
    public float[] transformPosX;
    public static bool onTrigger;
    public Camera cam;


    public int maxRoadCount = 2;
   // public int maxCarCount = 1;
    // Start is called before the first frame update
    void Start()
    {
        
        ResetRoad();
        NextCars();
    }

    // Update is called once per frame
    void Update()
    {
        if (onTrigger == true)
        {
            onTrigger = false;
            Destroy(roads[0]);
            roads.RemoveAt(0);
          
            NextRoad();

        }

        for (int i = 0; i < roads[0].transform.childCount; i++)
        {
            if (roads[0].GetComponentInChildren<Transform>().GetChild(i).transform.position.z < cam.transform.position.z - 5f 
                && !roads[0].GetComponentInChildren<Transform>().GetChild(i).gameObject.CompareTag("Road"))
            {
                roads[0].GetComponentInChildren<Transform>().GetChild(i).gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < cars[0].transform.childCount; i++)
        {
            if (cars[0].GetComponentInChildren<Transform>().GetChild(i).transform.position.z < cam.transform.position.z - 5f)
            {
                cars[0].GetComponentInChildren<Transform>().GetChild(i).gameObject.transform.position = new Vector3(transformPosX[Random.Range(0, 3)],
                    cars[0].GetComponentInChildren<Transform>().GetChild(i).gameObject.transform.position.y, cam.transform.position.z + 240f); ;
            }
        }

    }

    private void NextRoad()
    {
        Vector3 pos = new Vector3(RoadPrefab.transform.position.x, RoadPrefab.transform.position.y);
        if (roads.Count > 0)
        {
            pos = roads[roads.Count - 1].transform.position + new Vector3(0, 0, 157.7f);
        }
        GameObject obj = Instantiate(RoadPrefab, pos, Quaternion.identity);
        obj.transform.SetParent(transform);
        roads.Add(obj);
    }

    private void NextCars()
    {
        Vector3 pos2 = new Vector3(carPrefab.transform.position.x, carPrefab.transform.position.y, carPrefab.transform.position.z + 50f);
        GameObject obj = Instantiate(carPrefab, pos2, Quaternion.identity);
        obj.transform.SetParent(transform);
        cars.Add(obj);
    }




    public void ResetRoad()
    {
        while (roads.Count > 0)
        {
            Destroy(roads[0]);
            roads.RemoveAt(0);
        }
        for (int i = 0; i < maxRoadCount; i++)
        {
            NextRoad();
        }
    }


}
