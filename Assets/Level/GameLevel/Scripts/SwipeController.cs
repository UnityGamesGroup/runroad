﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeController : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    public GameObject player;
    public void OnBeginDrag(PointerEventData eventData)
    {
        if ((Mathf.Abs(eventData.delta.x)) > (Mathf.Abs(eventData.delta.y)))
        {
            if (eventData.delta.x < 0)
            {
                if (player.transform.position.x > -4.45f)
                {
                    player.transform.position = new Vector3(player.transform.position.x - 4.5f, player.transform.position.y, player.transform.position.z);
                }

            }
            else
            {
                if (player.transform.position.x < 4.45f)
                {
                    player.transform.position = new Vector3(player.transform.position.x + 4.5f, player.transform.position.y, player.transform.position.z);
                }
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
       
    }

    // Start is called before the first frame update
    void Awake()
    {
        player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
