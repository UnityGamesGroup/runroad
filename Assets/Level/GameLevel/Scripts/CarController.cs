﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{

    public GameObject[] cars;
    public float maxSpeed = 5f;
    public static float speed = 0;
    // Start is called before the first frame update
    void Start()
    {
        speed = maxSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (speed == 0) return;

        if (PlayerController.poins > 300 && PlayerController.poins < 600)
        {
            speed = 15;
        }
        else if (PlayerController.poins > 600 && PlayerController.poins < 1000)
        {
            speed = 20;
        }
        else if (PlayerController.poins > 1000 && PlayerController.poins < 1300)
        {
            speed = 25;
        }
        else if (PlayerController.poins > 1300 && PlayerController.poins < 1600)
        {
            speed = 30;
        }
        else if (PlayerController.poins > 1600 && PlayerController.poins < 2000)
        {
            speed = 35;
        }
        else if (PlayerController.poins > 2000)
        {
            speed = 40;
        }
        for (int i = 0; i < cars.Length; i++)
        {
            cars[i].transform.position -= new Vector3(0f, 0f, speed * Time.deltaTime);
        }
 
    }
}
